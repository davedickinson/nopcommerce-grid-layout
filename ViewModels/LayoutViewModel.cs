﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Plugin.Misc.GridLayout.Models;

namespace Nop.Plugin.Misc.GridLayout.ViewModels
{
    public class LayoutViewModel
    {
        public LayoutViewModel()
        {
          
        }

        /// <summary>
        /// Grid Group
        /// </summary>
        public GridGroup GridGroup { get; set; }

        /// <summary>
        /// Create Grid Item
        /// </summary>
        public GridItem CreateGridItem { get; set; }
    }
}
