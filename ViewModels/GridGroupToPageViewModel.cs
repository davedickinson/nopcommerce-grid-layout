﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Misc.GridLayout.ViewModels
{
    public class GridGroupToPageViewModel
    {
        public GridGroupToPageViewModel()
        {

        }

        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Grid Group Id
        /// </summary>
        public int GridGroupId { get; set; }

        /// <summary>
        /// Grid Group Title
        /// </summary>
        public string GridGroupTitle { get; set; }

        /// <summary>
        /// Entity Name
        /// </summary>
        public string EntityName { get; set; }

        /// <summary>
        /// Grid group shortcode
        /// </summary>
        public string GridGroupShortCode { get; set; }

        /// <summary>
        /// Is Active
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Link - the way the page is mapped?
        /// better if mapped via entity id and type if needs be.
        /// </summary>
        public string Link { get; set; }
   
        /// <summary>
        /// Created On
        /// </summary>
        public DateTime CreatedOn { get; set; }
    }
}
