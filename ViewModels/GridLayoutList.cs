﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Plugin.Misc.GridLayout.Models;

namespace Nop.Plugin.Misc.GridLayout.ViewModels
{
    /// <summary>
    /// Grid Layout List
    /// </summary>
    public class GridLayoutList
    {
        public IList<Layout> GridLayout { get; set; }
    }
}
