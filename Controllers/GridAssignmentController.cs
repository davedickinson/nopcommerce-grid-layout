﻿using System.Web.Mvc;
using Nop.Web.Framework.Controllers;
using Nop.Plugin.Misc.GridLayout.Models;
using Nop.Core.Data;
using Nop.Services.Catalog;
using Nop.Plugin.Misc.GridLayout.ViewModels;
using System.Collections.Generic;
using Nop.Core.Domain.Catalog;
using System.Linq;
using Nop.Services.Configuration;
using Nop.Services.Media;
using Nop.Web.Models.Catalog;
using Nop.Web.Models.Media;
using Nop.Core.Domain.Media;
using Nop.Services.Localization;
using Telerik.Web.Mvc;
using Nop.Admin.Models.Catalog;
using Nop.Services.Security;
using Nop.Core;
using Nop.Core.Domain;
using Nop.Core.Domain.Common;
using Nop.Admin;
using Nop.Web.Framework;
using System;
using Nop.Plugin.Misc.GridLayout.Services;
using Nop.Plugin.Misc.CategoryRoutes;
using Nop.Plugin.Misc.CategoryRoutes.Services;
using Nop.Plugin.Misc.CategoryRoutes.Models;  // Dependancy on another plugin :(

namespace Nop.Plugin.Misc.GridLayout.Controllers
{

    public class GridAssignmentController : Controller
    {
        private readonly IGridLayoutService _gridLayoutService;
        private readonly ICategoryService _categoryService;
        private readonly GridLayoutSettings _gridLayoutSettings;
        private readonly ISettingService _settingService;
        private readonly IPictureService _pictureService;
        private readonly IProductService _productService;
        private readonly MediaSettings _mediaSettings;
        private readonly ILocalizationService _localizationService;
        private readonly IWorkContext _workContext;
        private readonly IPermissionService _permissionService;
        private readonly AdminAreaSettings _adminAreaSettings;
        private readonly ICategoryRoutesService _categoryRoutesService;

        private readonly string _entityType = "category";


        public GridAssignmentController(ICategoryRoutesService categoryRoutesService, IGridLayoutService gridLayoutService, ICategoryService categoryService, GridLayoutSettings gridLayoutSettings, ISettingService settingService, IPictureService pictureService, IProductService productService, MediaSettings mediaSettings, ILocalizationService localizationService, IPermissionService permissionService, IWorkContext workContext, AdminAreaSettings adminAreaSettings)
        {
            _gridLayoutService = gridLayoutService;
            _gridLayoutSettings = gridLayoutSettings;
            _categoryService = categoryService;
            _settingService = settingService;
            _pictureService = pictureService;
            _productService = productService;
            _mediaSettings = mediaSettings;
          
            _localizationService = localizationService;
            _workContext = workContext;
            _permissionService = permissionService;
            _adminAreaSettings = adminAreaSettings;
            _categoryRoutesService = categoryRoutesService;
        }

        [AdminAuthorize]
        [HttpGet]
        public ActionResult Index()
        {
            return View("Nop.Plugin.Misc.GridLayout.Views.MiscGridAssignment.Index");
        }

        #region Ajax Methods

        /// <summary>
        /// Get Grid Layouts for a specified category / product
        /// </summary>
        /// <param name="gridItem"></param>
        /// <returns></returns>
        [GridAction]
        [AdminAuthorize]
        public JsonResult _GetAllGridGroupToPage()
        {
            return Json(this.PrepareGridGroupToPageViewModel(this._gridLayoutService.GetAllGridGroupToPage()));
        }

        /// <summary>
        /// Prepare Grid Group To Page View Model
        /// TODO : 
        /// Make Stored proceedure do this for us if this takes too long!
        /// </summary>
        /// <returns></returns>
        private IList<GridGroupToPageViewModel> PrepareGridGroupToPageViewModel(IList<GridGroupToPage> gridGroupsToPage)
        {
            IList<GridGroupToPageViewModel> gridGroupsToPageViewModels = new List<GridGroupToPageViewModel>();

            if (gridGroupsToPage != null)
            {
                foreach (var gridGroupToPage in gridGroupsToPage)
                {
                    GridGroupToPageViewModel gridGroupToPageViewModel = new GridGroupToPageViewModel();

                    if (!String.IsNullOrEmpty(gridGroupToPage.EntityType))
                    {
                        if (gridGroupToPage.EntityType.ToLower() == _entityType && gridGroupToPage.EntityId > 0)
                        {
                            Category category = _categoryService.GetCategoryById(gridGroupToPage.EntityId);
                           
                            if (category != null)
                            {
                                // Set the name
                                gridGroupToPageViewModel.EntityName = category.Name;

                                EntityRouteURL routeURL = this._categoryRoutesService.GetCategoryURLById(category.Id);
                                if (routeURL != null)
                                {
                                    // Set the link
                                    gridGroupToPageViewModel.Link = routeURL.Route;
                                }
                            }
                        }
                    }

                    // get the group
                    if (gridGroupToPage.GridGroup != null)
                    {
                        gridGroupToPageViewModel.GridGroupTitle = gridGroupToPage.GridGroup.Title;
                        gridGroupToPageViewModel.GridGroupShortCode = gridGroupToPage.GridGroup.ShortCode;
                    }
                    else
                    {
                        GridGroup gridGroup = _gridLayoutService.GetGridGroupById(gridGroupToPage.GridGroupId);
                        if (gridGroup != null)
                        {
                            gridGroupToPageViewModel.GridGroupTitle = gridGroup.Title;
                            gridGroupToPageViewModel.GridGroupShortCode = gridGroup.ShortCode;
                        }
                    }

                    gridGroupToPageViewModel.IsActive = gridGroupToPage.IsActive;
                    gridGroupToPageViewModel.Id = gridGroupToPage.Id;
                    gridGroupToPageViewModel.CreatedOn = gridGroupToPage.CreatedOn;
                 
                    // Add this to the list!
                    gridGroupsToPageViewModels.Add(gridGroupToPageViewModel);
                }
            }

            return gridGroupsToPageViewModels;
        }

        /// <summary>
        /// Get Grid Group to Page
        /// to add .... this is a post as it is being called via ajax 
        /// and it didnt seem to like gets.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [AdminAuthorize]
        public ActionResult _GetGridGroupToPage()
        {
            GridGroupToPage model = new GridGroupToPage(this._entityType);

            // For now set the entity type to be "Category" this follows the nopcommerce convention.
       
            // Create a GridGroupToPageViewModel
            GridGroupToPageDetailViewModel viewModel = new GridGroupToPageDetailViewModel();
            viewModel.GridGroupToPage = model;

            // Add Available Categories in drop down.
            viewModel.Categories = new List<SelectListItem>();
            viewModel.Categories.Add(new SelectListItem() { Text = "Choose a Category", Value = "0" });
            foreach (var category in _categoryService.GetAllCategories("", 0, int.MaxValue, true))
                viewModel.Categories.Add(new SelectListItem() { Text = category.Name, Value = category.Id.ToString() });

            // Add Available Grid Groups to the drop down - user their name and id.
            viewModel.GridGroups = new List<SelectListItem>();
            viewModel.GridGroups.Add(new SelectListItem() { Text = "Choose a Grid Layout", Value = "0" });
            foreach (var gridGroup in _gridLayoutService.GetAllGridGroups())
                viewModel.GridGroups.Add(new SelectListItem() { Text = gridGroup.Title, Value = gridGroup.Id.ToString() });

             return View("Nop.Plugin.Misc.GridLayout.Views.MiscGridAssignment.AddGridGroupToPage", viewModel);
        }

        /// <summary>
        /// Prepare Grid Group To Page Model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private GridGroupToPageDetailViewModel PrepareGridGroupToPageDetailViewModel(GridGroupToPage model)
        {
            GridGroupToPageDetailViewModel viewModel = new GridGroupToPageDetailViewModel();

            // Assign it back to the viewmodel.
            viewModel.GridGroupToPage = model;

            // Add Available Categories in drop down.
            viewModel.Categories = new List<SelectListItem>();
            viewModel.Categories.Add(new SelectListItem() { Text = "Choose a Category", Value = "0" });
            foreach (var category in _categoryService.GetAllCategories("", 0, int.MaxValue, true))
            {
           
                if (viewModel.GridGroupToPage != null && category.Id == viewModel.GridGroupToPage.EntityId)
                {
                    viewModel.Categories.Add(new SelectListItem() { Text = category.Name, Value = category.Id.ToString(), Selected = true });
                }
                else
                {
                    viewModel.Categories.Add(new SelectListItem() { Text = category.Name, Value = category.Id.ToString() });
                }
            }

            // Add Available Grid Groups to the drop down - user their name and id.
            viewModel.GridGroups = new List<SelectListItem>();
            viewModel.GridGroups.Add(new SelectListItem() { Text = "Choose a Grid Layout", Value = "0" });
            foreach (var gridGroup in _gridLayoutService.GetAllGridGroups())
            {
                if (viewModel.GridGroupToPage != null && gridGroup.Id == viewModel.GridGroupToPage.Id)
                {
                    viewModel.GridGroups.Add(new SelectListItem() { Text = gridGroup.Title, Value = gridGroup.Id.ToString(), Selected = true });
                }
                else
                {
                    viewModel.GridGroups.Add(new SelectListItem() { Text = gridGroup.Title, Value = gridGroup.Id.ToString() });
                }
            }

            return viewModel;
        }

        /// <summary>
        /// Get Grid Group to Page by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [AdminAuthorize]
        public ActionResult _GetGridGroupToPageById(int id)
        {
            GridGroupToPageDetailViewModel viewModel = null;

            if (id > 0)
            { 
                viewModel = this.PrepareGridGroupToPageDetailViewModel(this._gridLayoutService.GetGridGroupToPageById(id));
            }
            else
            {
                viewModel = this.PrepareGridGroupToPageDetailViewModel(null);
            }

            return View("Nop.Plugin.Misc.GridLayout.Views.MiscGridAssignment.EditGridGroupToPage", viewModel);
        }


        /// <summary>
        /// Get Grid Group to Page
        /// to add .... this is a post as it is being called via ajax 
        /// and it didnt seem to like gets.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [AdminAuthorize]
        public ActionResult _InsertGridGroupToPage(GridGroupToPageDetailViewModel viewmodel)
        {
            GridGroupToPageDetailViewModel updatedViewModel = null;

            if (ModelState.IsValid)
            {
                // This sets the id back on the viewmodel grid group to page
                this._gridLayoutService.CreateGridGroupToPage(viewmodel.GridGroupToPage);
                updatedViewModel = this.PrepareGridGroupToPageDetailViewModel(viewmodel.GridGroupToPage);
            }
            else
            {
                updatedViewModel = this.PrepareGridGroupToPageDetailViewModel(null);
            }

            // Return the edit view?
            return View("Nop.Plugin.Misc.GridLayout.Views.MiscGridAssignment.EditGridGroupToPage", updatedViewModel);             
        }

        /// <summary>
        /// Get Grid Group to Page
        /// to add .... this is a post as it is being called via ajax 
        /// and it didnt seem to like gets.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [AdminAuthorize]
        public ActionResult _UpdateGridGroupToPage(GridGroupToPageDetailViewModel viewmodel)
        {
              GridGroupToPageDetailViewModel updatedViewModel = null;

            if (ModelState.IsValid)
            {
                this._gridLayoutService.UpdateGridGroupToPage(viewmodel.GridGroupToPage);
                updatedViewModel = this.PrepareGridGroupToPageDetailViewModel(viewmodel.GridGroupToPage);
            }
            else
            {
                updatedViewModel = this.PrepareGridGroupToPageDetailViewModel(null);
            }

  
            // Return the edit view?
            return View("Nop.Plugin.Misc.GridLayout.Views.MiscGridAssignment.EditGridGroupToPage", updatedViewModel);
        }

        /// <summary>
        /// Delete Grid Group To Page
        /// TODO : Add success / failure json text to return
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [AdminAuthorize]
        public void _DeleteGridGroupToPage(int id)
        {
            if (id > 0)
            {
                this._gridLayoutService.DeleteGridGroupToPage(id);
            }        
        }

        #endregion
    }
}