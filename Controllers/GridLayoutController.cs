﻿using System.Web.Mvc;
using Nop.Web.Framework.Controllers;
using Nop.Plugin.Misc.GridLayout.Models;
using Nop.Core.Data;
using Nop.Services.Catalog;
using Nop.Plugin.Misc.GridLayout.ViewModels;
using System.Collections.Generic;
using Nop.Core.Domain.Catalog;
using System.Linq;
using Nop.Services.Configuration;
using Nop.Services.Media;
using Nop.Web.Models.Catalog;
using Nop.Web.Models.Media;
using Nop.Core.Domain.Media;
using Nop.Services.Localization;
using Telerik.Web.Mvc;
using Nop.Admin.Models.Catalog;
using Nop.Services.Security;
using Nop.Core;
using Nop.Core.Domain;
using Nop.Core.Domain.Common;
using Nop.Admin;
using Nop.Web.Framework;
using System;
using Nop.Plugin.Misc.GridLayout.Services;

namespace Nop.Plugin.Misc.GridLayout.Controllers
{
    /// <summary>
    /// TO DO : Split out into a category image manager controller and a product image controller
    ///         Create a service to deal with data collection and aggregation rather than doing the work in the controller.
    /// </summary>
    public class GridLayoutController : Controller
    {
        private readonly IGridLayoutService _gridLayoutService;
        private readonly ICategoryService _categoryService;
        private readonly GridLayoutSettings _gridLayoutSettings;
        private readonly ISettingService _settingService;
        private readonly IPictureService _pictureService;
        private readonly IProductService _productService;
        private readonly MediaSettings _mediaSettings;
        private readonly ILocalizationService _localizationService;
        private readonly IWorkContext _workContext;
        private readonly IPermissionService _permissionService;
        private readonly AdminAreaSettings _adminAreaSettings;
        private readonly string _entityType = "category";

        public GridLayoutController(IGridLayoutService gridLayoutService, ICategoryService categoryService, GridLayoutSettings gridLayoutSettings, ISettingService settingService, IPictureService pictureService, IProductService productService, MediaSettings mediaSettings, ILocalizationService localizationService, IPermissionService permissionService, IWorkContext workContext, AdminAreaSettings adminAreaSettings)
        {
            _gridLayoutService = gridLayoutService;
            _gridLayoutSettings = gridLayoutSettings;
            _categoryService = categoryService;
            _settingService = settingService;
            _pictureService = pictureService;
            _productService = productService;
            _mediaSettings = mediaSettings;
          
            _localizationService = localizationService;
            _workContext = workContext;
            _permissionService = permissionService;
            _adminAreaSettings = adminAreaSettings;
        }

        [AdminAuthorize]
        public ActionResult Configure()
        {
            var model = _gridLayoutSettings;
            return View("Nop.Plugin.Misc.GridLayout.Views.MiscGridLayout.Configure", model);
        }

        [AdminAuthorize]
        [HttpPost]
        public void _Configure(GridLayoutSettings formCollection)
        {
           // Set properties here - add to later
            _settingService.SaveSetting(_gridLayoutSettings);
        }

        #region Index View

        [AdminAuthorize]
        [HttpGet]
        public ActionResult Index()
        {
            return View("Nop.Plugin.Misc.GridLayout.Views.MiscGridLayout.Index");
        }

        #endregion

        #region Grid Layout / Group

        [HttpPost]
        [AdminAuthorize]
        public JsonResult _GetGridLayout(int gridGroupId)
        {
            if(gridGroupId > 0)
            {
                return Json(this._gridLayoutService.GetGridGroupById(gridGroupId));
            }

            return Json(String.Empty);
        }


        /// <summary>
        /// Get Grid Items
        /// </summary>
        /// <param name="gridGroupId"></param>
        /// <returns></returns>
        [GridAction]
        [AdminAuthorize]
        public JsonResult _GetAllGridGroups()
        {
            return Json(this._gridLayoutService.GetAllGridGroups().OrderBy(x=>x.CreatedOn).ToList());
        }


        [AdminAuthorize]
        [HttpGet]
        public ActionResult ManageLayout(int? id)
        {
            LayoutViewModel model = new LayoutViewModel();
            model.GridGroup = new GridGroup();
            model.CreateGridItem = new GridItem(id);

            if (ModelState.IsValid)
            {
                if (id.HasValue)
                {
                    GridGroup gridGroup = this._gridLayoutService.GetGridGroupById(id.Value);
                    model.CreateGridItem.GridGroupId = gridGroup.Id;
                    model.GridGroup = gridGroup;
                }           
            }
            return View("Nop.Plugin.Misc.GridLayout.Views.MiscGridLayout.Layout", model);
        }

        /// <summary>
        /// Update Layout, this should update all of the child objects as well
        /// check..
        /// </summary>
        /// <param name="gridGroupViewModel"></param>
        /// <returns></returns>
        [AdminAuthorize]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UpsertLayout(LayoutViewModel layoutViewModel)
        {
            if (ModelState.IsValid)
            {
                if (layoutViewModel.GridGroup.Id > 0)
                {
                    // It exists so lets update it 
                    this._gridLayoutService.UpdateGridGroup(layoutViewModel.GridGroup);
                }
                else 
                {
                    // It does not exist so lets create it.
                    this._gridLayoutService.CreateGridGroup(layoutViewModel.GridGroup);
                }
            }

            // set the grid item on the model again.
            layoutViewModel.CreateGridItem = new GridItem(layoutViewModel.GridGroup.Id);
            return View("Nop.Plugin.Misc.GridLayout.Views.MiscGridLayout.Layout", layoutViewModel);
        }

        /// <summary>
        /// Delete Layout
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AdminAuthorize]
        [HttpPost]
        public ActionResult DeleteLayout(int? id)
        {
            if (id.HasValue)
            {
                this._gridLayoutService.DeleteGridGroup(id.Value);
            }

            // Redirect to the main index page after deletion, check this does not need the full path!
            return RedirectToAction("Index");        
        }

        /// <summary>
        /// Delete Layout
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AdminAuthorize]
        [HttpGet]
        public ActionResult PreviewLayout(int gridGroupId)
        {
            GridGroup model = new GridGroup();

            if (gridGroupId > 0)
            {
                model = this._gridLayoutService.GetGridGroupById(gridGroupId);
            }

            return View("Nop.Plugin.Misc.GridLayout.Views.MiscGridLayout.PreviewLayout", model);
        }

        #region Return Partial View of a rendered grid layout

        /// <summary>
        /// Grid Layout
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public ViewResult _GridLayouts(int entityId)
        {
            IList<GridGroup> model = new List<GridGroup>();

            if (entityId > 0 && !String.IsNullOrEmpty(_entityType))
            {
                model = _gridLayoutService.GetGridGroups(entityId, _entityType);
            }

            return View("Nop.Plugin.Misc.GridLayout.Views.MiscGridLayout._GridLayout", model);
        }


        /// <summary>
        /// Get Grid Layouts By Params
        /// </summary>
        /// <param name="shortCode"></param>
        /// <returns></returns>
        public ViewResult _GridLayoutsByParams(string shortCode)
        {
            IList<GridGroup> model = new List<GridGroup>();

            if (!String.IsNullOrEmpty(shortCode))
            {
                model = this._gridLayoutService.GetGridGroupsByParams(shortCode);
            }

            return View("Nop.Plugin.Misc.GridLayout.Views.MiscGridLayout._GridLayout", model);
        }

        #region Has Grid Layout Action Methods

        /// <summary>
        /// Has Grid Layouts
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult HasGridLayouts(int? entityId)
        {
            bool hasAny = false;
            if(entityId.HasValue)
            {
                if ( entityId.Value > 0 && !String.IsNullOrEmpty(_entityType))
                {
                    hasAny = _gridLayoutService.HasGridGroups(entityId.Value, _entityType, true);
                }
            }

            ViewData["HasAny"] = hasAny;
            return View("Nop.Plugin.Misc.GridLayout.Views.MiscGridLayout.HasGridLayouts");
         
        }

        /// <summary>
        /// Has Grid Layouts
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult HasGridLayoutsByParams(string shortCode)
        {
            bool hasAny = false;

            if (!String.IsNullOrEmpty(shortCode))
            {
                hasAny = _gridLayoutService.HasGridGroupsByParams(shortCode);
            }

            ViewData["HasAny"] = hasAny;
            return View("Nop.Plugin.Misc.GridLayout.Views.MiscGridLayout.HasGridLayouts");
        }

        #endregion

        #endregion


        #region AJAX update methods

        #region GridGroup

        /// <summary>
        /// Delete Grid Item
        /// TODO : Add success / failure json text to return
        /// </summary>
        /// <param name="id"></param>
        [HttpPost]
        [AdminAuthorize]
        public void _DeleteGridGroup(int id)
        {
            if (id > 0)
            {
                this._gridLayoutService.DeleteGridGroup(id);
            }
        }


        #endregion

        /// <summary>
        /// Get Grid Layouts for a specified category / product
        /// </summary>
        /// <param name="gridItem"></param>
        /// <returns></returns>
        [HttpPost]
        [AdminAuthorize]
        public JsonResult _GetGridLayouts(int entityId)
        {
            if(entityId > 0 && String.IsNullOrEmpty(_entityType))
            {
                return Json(this._gridLayoutService.GetGridGroups(entityId, _entityType));
            }

            // Only for testing.
            return Json(String.Empty);
        }

       
        /// <summary>
        /// Create Grid Item
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AdminAuthorize]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult _InsertGridItem(GridItem gridItem)
        {  
            if (ModelState.IsValid)
            {
                this._gridLayoutService.CreateGridItem(gridItem);
            }

            // Return the edit view?
            return View("Nop.Plugin.Misc.GridLayout.Views.MiscGridLayout.EditGridItem", gridItem);
        }

        /// <summary>
        /// Get Grid Items
        /// </summary>
        /// <param name="gridGroupId"></param>
        /// <returns></returns>
        [GridAction]
        [AdminAuthorize]
        public JsonResult _GetGridItems(int gridGroupId)
        {
            return Json(this._gridLayoutService.GetGridItemsByGroupId(gridGroupId).OrderBy(x=>x.Seq).ToList());
        }

        /// <summary>
        /// Has Grid Layouts
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult _HasGridLayouts(int entityId)
        {
            bool hasAny = false;

            if (entityId > 0 && !String.IsNullOrEmpty(_entityType))
            {
                hasAny = _gridLayoutService.HasGridGroups(entityId, _entityType, true);
            }

            return Json(hasAny);
        }

        /// <summary>
        /// Has Grid Layouts
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult _HasGridLayoutsByParams(string shortCode)
        {
            bool hasAny = false;

            if (!String.IsNullOrEmpty(shortCode))
            {
                hasAny = _gridLayoutService.HasGridGroupsByParams(shortCode);
            }

            return Json(hasAny);
        }

        /// <summary>
        /// Add Grid Item
        /// Get a blank one first
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [AdminAuthorize]
        public ActionResult _GetGridItemByGroupId(int gridGroupId)
        {
            GridItem model = new GridItem(gridGroupId);
            return View("Nop.Plugin.Misc.GridLayout.Views.MiscGridLayout.AddGridItem", model);
        }

        /// <summary>
        /// Edit Grid Item
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [AdminAuthorize]
        public ActionResult _GetGridItem(int id)
        {
            GridItem model = this._gridLayoutService.GetGridItemById(id);
            return View("Nop.Plugin.Misc.GridLayout.Views.MiscGridLayout.EditGridItem", model);
        }
      
        /// <summary>
        /// Edit Grid Item
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [AdminAuthorize]
        [ValidateInput(false)]
        public ActionResult _UpdateGridItem(GridItem gridItem)
        {
            if (ModelState.IsValid)
            {
                this._gridLayoutService.UpdateGridItem(gridItem);
            }
            
            return View("Nop.Plugin.Misc.GridLayout.Views.MiscGridLayout.EditGridItem", gridItem);
        }
    
        /// <summary>
        /// Delete Grid Item
        /// TODO : Add success / failure json text to return
        /// </summary>
        /// <param name="id"></param>
        [HttpPost]
        [AdminAuthorize]
        public void _DeleteGridItem(int id)
        {
           this._gridLayoutService.DeleteGridItem(id);
        }

        #endregion

        #endregion

        #region Utilities

        /// <summary>
        /// Access denied view
        /// </summary>
        /// <returns>Access denied view</returns>
        protected ActionResult AccessDeniedView()
        {
            //return new HttpUnauthorizedResult();
            return RedirectToAction("AccessDenied", "Security", new { pageUrl = this.Request.RawUrl });
        }

        #endregion

    }
}