﻿using System.Web.Mvc;
using System.Web.Routing;
using Nop.Web.Framework.Mvc.Routes;

namespace Nop.Plugin.Misc.GridLayout
{
    public class ImageManagerRouteProvider : IRouteProvider
    {
        public void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute("Nop.Plugin.Misc.GridLayout.Index", "Admin/GridLayout/Admin/GridLayout/Index", new { controller = "GridLayout", action = "Index" }, new[] { "Nop.Plugin.Misc.GridLayout.Controllers" });
            routes.MapRoute("Nop.Plugin.Misc.GridLayout.ManageGridLayout", "Admin/GridLayout/Admin/GridLayout/Manage", new { controller = "GridLayout", action = "ManageLayout" }, new[] { "Nop.Plugin.Misc.GridLayout.Controllers" });
            routes.MapRoute("Nop.Plugin.Misc.GridLayout.UpsertGridLayout", "Admin/GridLayout/Admin/GridLayout/UpsertLayout", new { controller = "GridLayout", action = "UpsertLayout" }, new[] { "Nop.Plugin.Misc.GridLayout.Controllers" });
            routes.MapRoute("Nop.Plugin.Misc.GridLayout.Assignment.Index", "Admin/GridLayout/Admin/GridAssignment/Index", new { controller = "GridAssignment", action = "Index" }, new[] { "Nop.Plugin.Misc.GridLayout.Controllers" });
        }
        public int Priority
        {
            get { return 0; }
        }
    }
}
