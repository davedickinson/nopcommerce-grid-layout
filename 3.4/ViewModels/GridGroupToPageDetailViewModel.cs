﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Plugin.Misc.GridLayout.Models;
using System.Web.Mvc;
using System.ComponentModel;

namespace Nop.Plugin.Misc.GridLayout.ViewModels
{
    /// <summary>
    /// Grid Group To Page View Model
    /// </summary>
    public class GridGroupToPageDetailViewModel
    {
        public GridGroupToPageDetailViewModel()
        {
            // var model = new Nop.Plugin.Misc.OrderTracking.Models.OrderTrackingListModel();
            // model.AvailableOrderStatuses = OrderStatus.Pending.ToSelectList(false).ToList();
           // model.AvailableOrderStatuses.Insert(0, new SelectListItem() { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
        }

        /// <summary>
        /// Grid Group To Page
        /// </summary>
        public IList<GridGroupToPage> GridGroupToPages { get; set; }

        /// <summary>
        /// Categories
        /// </summary>
        public IList<SelectListItem> Categories { get; set; }

        /// <summary>
        /// Grid Groups
        /// </summary>
        [DisplayName("Grid Layouts")]
        public IList<SelectListItem> GridGroups { get; set; }

        /// <summary>
        /// Products
        /// </summary>
        public IList<SelectListItem> Products { get; set; }

        /// <summary>
        /// Grid Group To Page
        /// </summary>
        public GridGroupToPage GridGroupToPage { get; set; }
 
    }
}
