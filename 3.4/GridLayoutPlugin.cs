﻿using System.Web.Routing;
using Nop.Core.Plugins;
using Nop.Services.Common;
using Nop.Plugin.Misc.GridLayout.Data;
using Nop.Services.Configuration;
using Nop.Plugin.Misc.GridLayout.Models;
using Nop.Web.Framework.Web;
using Nop.Web.Framework.Menu;

namespace Nop.Plugin.Misc.GridLayout
{
    public class ImageManagerPlugin : BasePlugin, IMiscPlugin, IAdminMenuPlugin
    {
        private readonly ISettingService _settingService;
        private readonly GridLayoutModelObjectContext _context;

        public ImageManagerPlugin(GridLayoutModelObjectContext context, ISettingService settingService)
        {
            _context = context;
            _settingService = settingService;
        }

        /// <summary>
        /// Gets a route for provider configuration
        /// </summary>
        /// <param name="actionName">Action name</param>
        /// <param name="controllerName">Controller name</param>
        /// <param name="routeValues">Route values</param>
        public void GetConfigurationRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "Configure";
            controllerName = "GridLayout";
            routeValues = new RouteValueDictionary() { { "Namespaces", "Nop.Plugin.Misc.GridLayout.Controllers" }, { "area", null } };
        }


        /// <summary>
        /// Build Menu Item
        /// </summary>
        public SiteMapNode BuildMenuItem()
        {
            SiteMapNode siteMapNode = new SiteMapNode();
            siteMapNode.Visible = true;
            siteMapNode.Title = "Grid Layout";
            siteMapNode.Url = "/Admin/GridLayout/Admin/GridLayout/Index";
            return siteMapNode;
        }

        public bool Authenticate()
        {
            return true;
        }


        public override void Install()
        {
            _context.Install();
            base.Install();
        }

        /// <summary>
        /// Uninstall plugin
        /// </summary>
        public override void Uninstall()
        {
            _settingService.DeleteSetting<GridLayoutSettings>();
            _context.Uninstall();
            base.Uninstall();
        }
    }
}