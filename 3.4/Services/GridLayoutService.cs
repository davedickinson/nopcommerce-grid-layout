﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Plugin.Misc.GridLayout.Data;
using Nop.Plugin.Misc.GridLayout.Models;
using Autofac;
using Autofac.Core;
using Autofac.Integration.Mvc;
using Nop.Core.Data;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Data;
using Layout = Nop.Plugin.Misc.GridLayout.Models.Layout;
using Nop.Services.Media;

namespace Nop.Plugin.Misc.GridLayout.Services
{
    public class GridLayoutService : IGridLayoutService
    {
       private readonly IRepository<GridGroup> _gridGroupRepository;
       private readonly IRepository<GridItem> _gridItemRepository;
       private readonly IRepository<GridGroupToPage> _gridGroupToPageRepository;
       private readonly IPictureService _pictureService;

        public GridLayoutService(IPictureService pictureService, IRepository<GridGroupToPage> gridGroupToPageRepository, IRepository<GridItem> gridItemRepository, IRepository<GridGroup> gridGroupRepository)
        {
            _gridGroupToPageRepository = gridGroupToPageRepository;
            _gridItemRepository = gridItemRepository;
            _gridGroupRepository = gridGroupRepository;
            _pictureService = pictureService;
        }

        /* TODO to get complex entites...
         *  var query = Context.Roles
               .Include("Users")
               .Where(r => r.ID == id)
               .Select(r => new RoleDetail() {
                  ID = r.ID,
                  Rolename = r.Rolename,
                  Active = r.Active,
                  Users = r.Users
                           .Select(u => new RoleDetail.RoleUser() {
                              ID = u.ID,
                              Username = u.Username
                            })
                            // .ToList()
                })
               .FirstOrDefault();
        */

        /// <summary>
        /// Has Grid groups
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool HasGridGroups(int entityId, string entityType, bool isActive)
        {
            bool hasAny = false;

            if (entityId > 0 && !String.IsNullOrEmpty(entityType))
            {
                // Get a list of supplier products variants that are mapped to this product
                var query = from ggp in _gridGroupToPageRepository.Table
                            where ggp.EntityId == entityId &&
                            ggp.EntityType.ToLower().Trim() == entityType.ToLower().Trim() &&
                            ggp.IsActive == isActive 
                            select ggp;

                if (query != null)
                {
                    hasAny = query.ToList().Count() > 0 ? true : false;
                }
            }

            return hasAny;
        }

        /// <summary>
        /// Has Grid groups
        /// by params (extend this out to be an object if needs be)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool HasGridGroupsByParams(string shortCode, bool isActive)
        {
            bool hasAny = false;

            if (!String.IsNullOrEmpty(shortCode))
            {
                // Get a list of supplier products variants that are mapped to this product
                var query = from ggp in _gridGroupToPageRepository.Table
                            where ggp.GridGroup.ShortCode.ToLower().Trim() == shortCode.ToLower().Trim()
                            && ggp.IsActive == isActive
                            select ggp.GridGroup;
                            // select new {ResolvedOn=g.Key, NumberResolved= g.Count()}

                if (query != null)
                {
                    hasAny = query.ToList().Count() > 0 ? true : false;
                }
            }

            return hasAny;
        }

        /// <summary>
        /// Get Grid Layouts
        /// TODO : This could be improved as we can get grid group from the grid group to page mapping.
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="entityType"></param>
        /// <returns></returns>
        public IList<GridGroup> GetGridGroupsByParams(string shortCode, bool isActive)
        {
            IList<GridGroup> gridGroups = new List<GridGroup>();
            if (!String.IsNullOrEmpty(shortCode))
            {
                // Get a list of supplier products variants that are mapped to this product
                var query = from ggp in _gridGroupToPageRepository.Table
                            where ggp.GridGroup.ShortCode.ToLower().Trim() == shortCode.ToLower().Trim() 
                            && ggp.IsActive == isActive
                            select ggp.GridGroup;

                if (query != null)
                {
                    // Only get one grid group that matches - provided it is active.
                    GridGroup gridGroup = query.FirstOrDefault();
 
                    if (gridGroup != null)
                    {
                        // Sort out the image url path here...?
                        gridGroup.Items =  this.PrepareGridItems(gridGroup.Items.OrderBy(x=>x.Seq).ToList());
                      
                        gridGroups.Add(gridGroup);
                    }
                }
            }

            return gridGroups;
        }

        /// <summary>
        /// Get Grid Layouts
        /// </summary>
        /// <param name="entityId"></param>
        /// <param name="entityType"></param>
        /// <returns></returns>
        public IList<GridGroup> GetGridGroups(int entityId, string entityType)
        {
            IList<GridGroup> gridGroups = new List<GridGroup>();
            if(entityId > 0 && !String.IsNullOrEmpty(entityType))
            {
                // Get a list of supplier products variants that are mapped to this product
                var query = from ggp in _gridGroupToPageRepository.Table
                            where ggp.EntityId == entityId && 
                            ggp.EntityType.ToLower().Trim() == entityType.ToLower().Trim() &&
                            ggp.IsActive
                            select ggp;

                if (query != null)
                {
                   var  gridGroupPages = query.ToList();

                   foreach (var gridGroupToPage in gridGroupPages)
                    {
                        if(gridGroupToPage != null)
                        {
                            GridGroup gridGroup = this.GetGridGroupById(gridGroupToPage.GridGroupId);
                            if (gridGroup != null)
                            {
                                gridGroups.Add(gridGroup);
                            }
                        }
                    }
                }
            }

            return gridGroups.ToList();
        }
        
        /// <summary>
        /// Get All Grid Groups
        /// </summary>
        /// <returns></returns>
        public IList<Layout> GetAllGridLayouts()
        {
            IList<Layout> gridLayouts = new List<Layout>();
            IList<GridGroup> gridGroups = new List<GridGroup>();

            // Get a list of supplier products variants that are mapped to this product
            var query = from gg in _gridGroupRepository.Table
                        select gg;
            if(query != null)
            {
                gridGroups = query.ToList();

                foreach (var gridGroup in gridGroups)
                {
                    Layout gridLayout = new Layout();
                    gridLayout.GridGroup = gridGroup;
                    gridLayouts.Add(gridLayout);
                }
            }

            return gridLayouts.ToList();
        }

        #region Grid Groups

        /// <summary>
        /// Get All Grid Groups
        /// </summary>
        /// <returns></returns>
        public IList<GridGroup> GetAllGridGroups()
        {
            IList<GridGroup> gridGroups = new List<GridGroup>();

            // Get a list of supplier products variants that are mapped to this product
            var query = from gg in _gridGroupRepository.Table
                        select gg;
            if(query != null)
            {
                gridGroups = query.ToList();

                foreach (var gridGroup in gridGroups)
                {
                    gridGroup.Items = this.GetGridItemsByGroupId(gridGroup.Id);
                }
 
            }

            return gridGroups.ToList();
        }

        /// <summary>
        /// Get Grid Group By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public GridGroup GetGridGroupById(int id)
        {
            GridGroup gridGroup = null;
            if (id > 0)
            {
                var query = from gg in _gridGroupRepository.Table
                            where gg.Id == id
                            select gg;
                if (query != null)
                {
                    gridGroup = query.FirstOrDefault();
                    if (gridGroup != null)
                    {
                        // TODO : Look into mapping this automatically.
                        // For now, get griditems by the group id and graft these on here.
                        gridGroup.Items = this.GetGridItemsByGroupId(gridGroup.Id);
                    }
                }
            }

            return gridGroup;
        }

        /// <summary>
        /// Update Grid Group
        /// Again, update was not working for me.
        /// loosing tracking on ef?
        /// What am i doing differently here?
        /// </summary>
        /// <param name="gridGroup"></param>
        public void UpdateGridGroup(GridGroup gridGroup)
        {
           if(gridGroup != null)
           {
  
                   // Entry(account).State = EntityState.Modified;
                   GridGroup old = (from gg in _gridGroupRepository.Table
                                    where gg.Id == gridGroup.Id
                                   select gg).SingleOrDefault();

                    old.Title = gridGroup.Title;
                    old.Description = gridGroup.Description;
                    old.BaseItemWidth = gridGroup.BaseItemWidth;
                    old.BaseItemHeight = gridGroup.BaseItemHeight;
                    old.NumberOfColumns = gridGroup.NumberOfColumns;
                    old.ShortCode = gridGroup.ShortCode;

                   this._gridGroupRepository.Update(old);
               }

              // _gridGroupRepository.Update(gridGroup);
         
        }
            
        /// <summary>
        /// Create Grid Group
        /// This wont have an id.
        /// </summary>
        /// <param name="gridGroup"></param>
        public void CreateGridGroup(GridGroup gridGroup)
        {
 	       if(gridGroup != null)
           {
                gridGroup.CreatedOn = DateTime.Now;
               _gridGroupRepository.Insert(gridGroup);
           }
        }

        /// <summary>
        /// Delete Grid Group
        /// </summary>
        /// <param name="id"></param>
        public void DeleteGridGroup(int id)
        {
            // Check to see if the grid group exists first before deleting it?
            try
            {
                if (id > 0)
                {
                    var query = from gg in _gridGroupRepository.Table
                                where gg.Id == id
                                select gg;

                    if (query != null)
                    {
                        GridGroup gridGroup = query.FirstOrDefault();
                        _gridGroupRepository.Delete(gridGroup);
                    }
                }
            }
            catch(Exception e)
            {

            }
        }

        #endregion

        #region Grid Item

        /// <summary>
        /// Get Grid Items By Group Id
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public IList<GridItem> GetGridItemsByGroupId(int groupId)
        {
            IList<GridItem> gridItems = new List<GridItem>();

            if (groupId > 0)
            {
                var query = from gi in _gridItemRepository.Table
                            where gi.GridGroupId == groupId
                            orderby gi.Seq
                            select gi;

                if (query != null)
                {
                    gridItems = this.PrepareGridItems(query.ToList());
                }
            }

            return gridItems;
        }

        /// <summary>
        /// Prepare Grid Items
        /// </summary>
        /// <param name="gridItems"></param>
        /// <returns></returns>
        private IList<GridItem> PrepareGridItems(IList<GridItem> gridItems)
        {
            if (gridItems != null)
            {
                foreach (var gridItem in gridItems)
                {
                    this.PrepareGridItem(gridItem);
                }

                return gridItems.OrderBy(x => x.Seq).ToList();
            }

            return gridItems;
 
        }

        /// <summary>
        /// Prepare Grid Item
        /// </summary>
        /// <param name="gridItem"></param>
        /// <returns></returns>
        private GridItem PrepareGridItem(GridItem gridItem)
        {
            if (gridItem != null)
            {
                string image = String.Empty;

                if (!String.IsNullOrEmpty(gridItem.ImageURL))
                {
                    image = gridItem.ImageURL;
                }
                else if (gridItem.ImageId > 0)
                {
                    image = this._pictureService.GetPictureUrl(gridItem.ImageId);
                }

                // Set the image url back on to the gridItem.
                gridItem.Image = image;
            }

            return gridItem;
        }

        /// <summary>
        /// Get Grid Item
        /// </summary>
        /// <param name="gridItem"></param>
        public GridItem GetGridItemById(int id)
        {
            GridItem gridItem = null;

            if (id > 0)
            {
                gridItem = this.PrepareGridItem(_gridItemRepository.GetById(id));
            }

            return gridItem;
        }

        /// <summary>
        /// Update Grid Item
        /// Could not get this to save- was not behaving itself.
        /// I may somewhere be passing an object reference back and so we are loosing the context.
        /// Attached/Detached entity state.
        /// Work around for now, investigate later
        /// http://stackoverflow.com/questions/8056761/context-savechanges-not-working
        /// </summary>
        /// <param name="gridItem"></param>
        public void UpdateGridItem(GridItem gridItem)
        {
              if(gridItem != null)
              {
                // Entry(account).State = EntityState.Modified;
                  GridItem old = (from gi in _gridItemRepository.Table
                                  where gi.Id == gridItem.Id
                                  select gi).SingleOrDefault();

                old.Content = gridItem.Content;
                old.GridGroupId = gridItem.GridGroupId;
                old.Title = gridItem.Title;
                old.URL = gridItem.URL;
                old.ItemTypeId = gridItem.ItemTypeId;
                old.ImageId = gridItem.ImageId;
                old.ImageURL = gridItem.ImageURL;
                old.Image = gridItem.Image;
                old.ItemsWide = gridItem.ItemsWide;
                old.ItemsHigh = gridItem.ItemsHigh;
                old.Seq = gridItem.Seq;

                this._gridItemRepository.Update(old);               
              }
        }

        /// <summary>
        /// Create Grid Item
        /// </summary>
        /// <param name="gridItem"></param>
        public void CreateGridItem(GridItem gridItem)
        {
 	        if(gridItem != null)
            {
                _gridItemRepository.Insert(gridItem);
            }
        }

        /// <summary>
        /// Delete Grid Item
        /// check it exists first
        /// </summary>
        /// <param name="id"></param>
        public void DeleteGridItem(int id)
        {
 	       if(id > 0)
           {
               GridItem gridItem = _gridItemRepository.GetById(id);
               if (gridItem != null)
               { 
                   _gridItemRepository.Delete(gridItem);
               }
           }
        }
        
        #endregion

        #region Grid Group To page

        /// <summary>
        /// Update Grid Group to Page
        /// </summary>
        /// <param name="gridGroupToPage"></param>
        public void UpdateGridGroupToPage(GridGroupToPage gridGroupToPage)
        {
            if (gridGroupToPage != null)
            {
                // Entry(account).State = EntityState.Modified;
                GridGroupToPage old = (from ggp in _gridGroupToPageRepository.Table
                                        where ggp.Id == gridGroupToPage.Id
                                        select ggp).SingleOrDefault();

                if (old != null)
                {
                    old.GridGroupId = gridGroupToPage.GridGroupId;
                    old.IsActive = gridGroupToPage.IsActive;
                    old.Link = gridGroupToPage.Link;
                    old.EntityId = gridGroupToPage.EntityId;

                    this._gridGroupToPageRepository.Update(old);
                }
            }

            //_gridGroupToPageRepository.Update(gridGroupToPage);    
        }

        /// <summary>
        /// Create Grid Group to Page
        /// </summary>
        /// <param name="gridGroupToPage"></param>
        public void CreateGridGroupToPage(GridGroupToPage gridGroupToPage)
        {
            if (gridGroupToPage != null)
            {
                gridGroupToPage.CreatedOn = DateTime.Now;
                _gridGroupToPageRepository.Insert(gridGroupToPage);
            }
        }

        /// <summary>
        /// Delete Grid Group to Page
        /// </summary>
        /// <param name="id"></param>
        public void DeleteGridGroupToPage(int id)
        {
 	        if(id > 0 )
            {
                GridGroupToPage gridGroupToPage = _gridGroupToPageRepository.GetById(id);
                _gridGroupToPageRepository.Delete(gridGroupToPage);
            }
        }

        /// <summary>
        /// Get Grid Group To Page
        /// </summary
        /// <param name="id"></param>
        public GridGroupToPage GetGridGroupToPageById(int id)
        {
            GridGroupToPage gridGroupToPage = null;

            if(id > 0)
            {
                return _gridGroupToPageRepository.GetById(id);
            }

            return gridGroupToPage;
        }

        /// <summary>
        /// Get All Grid To Groups
        /// </summary>
        /// <returns></returns>
        public IList<GridGroupToPage> GetAllGridGroupToPage()
        {
            IList<GridGroupToPage> gridGroupToPage = new List<GridGroupToPage>();
            var query = from ggp in _gridGroupToPageRepository.Table               
                        select ggp;

            if (query != null)
            {
                return query.ToList();
            }

            return gridGroupToPage;
        }

        #endregion
    }
}
