﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Plugin.Misc.GridLayout.Models;
using Layout = Nop.Plugin.Misc.GridLayout.Models.Layout;
namespace Nop.Plugin.Misc.GridLayout.Services
{
    public interface IGridLayoutService
    {
        IList<GridGroup> GetAllGridGroups();
        IList<Layout> GetAllGridLayouts();
        IList<GridGroup> GetGridGroups(int entityId, string entityType);
        IList<GridGroup> GetGridGroupsByParams(string shortCode, bool isActive = true);
        void CreateGridGroup(GridGroup gridGroup);
        void UpdateGridGroup(GridGroup gridGroup);
        bool HasGridGroups(int entityId, string entityType, bool isActive = true);
        bool HasGridGroupsByParams(string shortCode, bool isActive = true);
        void DeleteGridGroup(int id);
        GridGroup GetGridGroupById(int id);
        void CreateGridItem(GridItem gridItem);
        void UpdateGridItem(GridItem gridItem);
        void DeleteGridItem(int id);
        GridItem GetGridItemById(int id);
        void CreateGridGroupToPage(GridGroupToPage gridGroupToPage);
        void UpdateGridGroupToPage(GridGroupToPage gridGroupToPage);
        void DeleteGridGroupToPage(int id);
        GridGroupToPage GetGridGroupToPageById(int id);
        IList<GridItem> GetGridItemsByGroupId(int gridGroupId);
        IList<GridGroupToPage> GetAllGridGroupToPage();
    }
}
