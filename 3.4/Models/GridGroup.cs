﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace Nop.Plugin.Misc.GridLayout.Models
{
    /// <summary>
    /// Grid Group
    /// </summary>
    public class GridGroup : BaseEntity
    {
        public GridGroup()
        {
            this.NumberOfColumns = 3;
            this.Title = String.Empty;
            this.Description = String.Empty;
            
            // Create an empty list.
            this.Items = new List<GridItem>();
        }

        /// <summary>
        /// Items
        /// </summary>
        public virtual IList<GridItem> Items { get; set; }

        /// <summary>
        /// ShortCode
        /// </summary>
        public virtual string ShortCode { get; set; }

        [NotMapped]
        public string ItemsCount { 
            
            get 
            { 
                int itemsCount = 0;
                if(this.Items != null)
                {
                    itemsCount = this.Items.Count();
                }

                return itemsCount.ToString();
            }
        }

        /// <summary>
        /// Title
        /// </summary>
        [Required]
        public virtual string Title { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        public virtual string Description { get; set; }

        /// <summary>
        /// Default Item Width
        /// </summary>
        public virtual int BaseItemWidth { get; set; }

        /// <summary>
        /// Default Item Height
        /// </summary>
    
        public virtual int BaseItemHeight { get; set; }

        /// <summary>
        /// Columns 
        /// </summary>
        public virtual int NumberOfColumns { get; set; }

        /// <summary>
        /// Created On
        /// </summary>
        [DisplayName("Created On")]
        public virtual DateTime CreatedOn { get; set; }

    }
}
