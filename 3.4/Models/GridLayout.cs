﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Misc.GridLayout.Models
{
    /// <summary>
    /// Layout
    /// </summary>
    public class Layout
    {
        public Layout() { }
        public int ActiveOccurances { get; set; }
        public GridGroup GridGroup { get; set; }    
    }
}
