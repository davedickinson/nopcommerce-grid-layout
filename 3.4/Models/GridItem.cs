﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace Nop.Plugin.Misc.GridLayout.Models
{
    /// <summary>
    /// Grid Item
    /// </summary>
   public class GridItem : BaseEntity
   {
       public GridItem()
       {
           this.Title = String.Empty;
           this.Content = String.Empty;
           this.URL = String.Empty;
           this.ImageId = 0;
           this.ImageURL = String.Empty;
           this.ItemsWide = 1;
           this.ItemsHigh = 1;
           this.Seq = 1;
           this.GridGroupId = 0;
       }

        public GridItem(int? gridGroupId) : this()
        {
            this.GridGroupId = gridGroupId.HasValue ? gridGroupId.Value : 0;   
        }

        public virtual string Content { get; set; }
        public virtual int GridGroupId { get; set; }
        public virtual string Title { get; set; }
        public virtual string URL { get; set; }

        [DisplayName("Style Type")]
        public int ItemTypeId { get; set; }

       [UIHint("Picture")]
       [DisplayName("Image")]
       public virtual int ImageId { get; set; }
       public virtual string ImageURL { get; set; }

       [NotMapped]
       public string Image { get; set; }

       [NotMapped]
       public GridItemType Type { 
           get 
           {
               return (GridItemType)ItemTypeId;         
           } 
       }
       
       [NotMapped]
       public string TypeText { 
           get
           {
               var typeText = string.Empty;
               if(this.ItemTypeId > 0)
               {
                   typeText = this.Type.ToString();
               }

               return typeText;
           }
       }

       [DisplayName("Items Wide")]
       public virtual int ItemsWide { get; set; }

       [DisplayName("Items High")]
       public virtual int ItemsHigh { get; set; }

       [DisplayName("Order")]
       public virtual int Seq { get; set; }
    }
}
