﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace Nop.Plugin.Misc.GridLayout.Models
{
    /// <summary>
    /// Grid Group to Page
    /// </summary>
    public class GridGroupToPage : BaseEntity
    {
        public GridGroupToPage()
        {
            this.Link = String.Empty; 
        }

        public GridGroupToPage(string entityType) : base()
        {
            this.EntityType = entityType;
        }

        /// <summary>
        /// Grid Group Id
        /// </summary>
        [DisplayName("Grid Layout")]
        public virtual int GridGroupId { get; set; }

        /// <summary>
        /// Grid Group
        /// </summary>
        public virtual GridGroup GridGroup { get; set; }

        /// <summary>
        /// Is Active
        /// </summary>
        [DisplayName("Is Active")]
        public bool IsActive { get; set; }

        /// <summary>
        /// Link - the way the page is mapped?
        /// better if mapped via entity id and type if needs be.
        /// </summary>
        public string Link { get; set; }

        /// <summary>
        /// Entity Id
        /// </summary>
        public int EntityId { get; set; }

        /// <summary>
        /// Entity Type
        /// </summary>
        [DisplayName("Type")]
        public string EntityType { get; set; }

        /// <summary>
        /// Created On
        /// </summary>
        public DateTime CreatedOn { get; set; }

    }
}
