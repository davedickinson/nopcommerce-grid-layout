﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Misc.GridLayout.Models
{
    public enum GridItemType : int
    {
        /// <summary>
        /// Default
        /// </summary>
        Default = 0,
        /// <summary>
        /// Full
        /// </summary>
        Full = 10,
        /// <summary>
        /// Overlay
        /// </summary>
        Overlay = 20,
        /// <summary>
        /// Heading
        /// </summary>
        Heading = 30
      
    }
}
