﻿using Autofac;
using Autofac.Core;
using Autofac.Integration.Mvc;
using Nop.Core.Data;
using Nop.Core.Infrastructure;
using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Data;
using Nop.Plugin.Misc.CategoryRoutes.Services;
using Nop.Plugin.Misc.GridLayout.Data;
using Nop.Plugin.Misc.GridLayout.Models;
using Nop.Plugin.Misc.GridLayout.Services;

namespace Nop.Plugin.Misc.GridLayout
{
    public class GridLayoutDependencyRegistrar : IDependencyRegistrar
    {
        private const string CONTEXT_NAME = "nop_object_context_gridlayout";

        #region Implementation of IDependencyRegistrar

        public void Register(ContainerBuilder builder, ITypeFinder typeFinder)
        {
            //Load custom data settings
            var dataSettingsManager = new DataSettingsManager();
            DataSettings dataSettings = dataSettingsManager.LoadSettings();

            //Register services
            builder.RegisterType<GridLayoutService>().As<IGridLayoutService>();
            builder.RegisterType<CategoryRoutesService>().As<ICategoryRoutesService>(); // Dependancy on category routes - should use it as a dll.

            //Register custom object context
            builder.Register<IDbContext>(c => RegisterIDbSupplierObjectContext(c, dataSettings)).Named<IDbContext>("nop_object_context_gridlayout").InstancePerHttpRequest();
            builder.Register(c => RegisterIDbSupplierObjectContext(c, dataSettings)).InstancePerHttpRequest();

            //Override the repository injection - 
            builder.RegisterType<EfRepository<GridGroup>>().As<IRepository<GridGroup>>().WithParameter(ResolvedParameter.ForNamed<IDbContext>("nop_object_context_gridlayout")).InstancePerHttpRequest();
            builder.RegisterType<EfRepository<GridGroupToPage>>().As<IRepository<GridGroupToPage>>().WithParameter(ResolvedParameter.ForNamed<IDbContext>("nop_object_context_gridlayout")).InstancePerHttpRequest();
            builder.RegisterType<EfRepository<GridItem>>().As<IRepository<GridItem>>().WithParameter(ResolvedParameter.ForNamed<IDbContext>("nop_object_context_gridlayout")).InstancePerHttpRequest();
      
        }

        #endregion

        #region Implementation of IDependencyRegistrar

        public int Order
        {
            get { return 0; }
        }

        #endregion

        /// <summary>
        /// Registers the I db context.
        /// </summary>
        /// <param name="componentContext">The component context.</param>
        /// <param name="dataSettings">The data settings.</param>
        /// <returns></returns>
        private GridLayoutModelObjectContext RegisterIDbSupplierObjectContext(IComponentContext componentContext, DataSettings dataSettings)
        {
            string dataConnectionStrings;

            if (dataSettings != null && dataSettings.IsValid())
            {
                dataConnectionStrings = dataSettings.DataConnectionString;
            }
            else
            {
                dataConnectionStrings = componentContext.Resolve<DataSettings>().DataConnectionString;
            }

            return new GridLayoutModelObjectContext(dataConnectionStrings);
        }
    }
}