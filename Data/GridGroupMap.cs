﻿using System.Data.Entity.ModelConfiguration;
using Nop.Plugin.Misc.GridLayout.Models;

namespace Nop.Plugin.Misc.GridLayout.Data
{
    public class GridGroupModelObjectMap : EntityTypeConfiguration<GridGroup>
    {
        public GridGroupModelObjectMap()
        {
            ToTable("PWGridLayout_Group");

            //Map the primary key
            HasKey(m => m.Id);


        }
    }
}
