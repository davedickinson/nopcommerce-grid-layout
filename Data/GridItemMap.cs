﻿using System.Data.Entity.ModelConfiguration;
using Nop.Plugin.Misc.GridLayout.Models;


namespace Nop.Plugin.Misc.GridLayout.Data
{
    public class GridItemModelObjectMap : EntityTypeConfiguration<GridItem>
    {
        public GridItemModelObjectMap()
        {
            ToTable("PWGridLayout_Item");

            //Map the primary key
            HasKey(m => m.Id);

        }
    }
}
