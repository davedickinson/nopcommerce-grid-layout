﻿using System.Data.Entity.ModelConfiguration;
using Nop.Plugin.Misc.GridLayout.Models;


namespace Nop.Plugin.Misc.GridLayout.Data
{
    public class GridGroupToPageModelObjectMap : EntityTypeConfiguration<GridGroupToPage>
    {
        public GridGroupToPageModelObjectMap()
        {
            ToTable("PWGridLayout_GroupToPage");

            //Map the primary key
            HasKey(m => m.Id);

        }
    }
}
