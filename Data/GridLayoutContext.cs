﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Nop.Core;
using Nop.Data;
using System.Text;

namespace Nop.Plugin.Misc.GridLayout.Data
{
    public class GridLayoutModelObjectContext : DbContext, IDbContext
    {
        public GridLayoutModelObjectContext(string nameOrConnectionString) : base(nameOrConnectionString) { }

        #region Implementation of IDbContext

        #endregion

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new GridGroupModelObjectMap());
            modelBuilder.Configurations.Add(new GridGroupToPageModelObjectMap());
            modelBuilder.Configurations.Add(new GridItemModelObjectMap());

            // More maps for gridlayout map
            base.OnModelCreating(modelBuilder);
        }

        public string CreateDatabaseInstallationScript()
        {
            return ((IObjectContextAdapter)this).ObjectContext.CreateDatabaseScript();
        }

        public void Install()
        {
            //It's required to set initializer to null (for SQL Server Compact).
            //otherwise, you'll get something like "The model backing the 'your context name' context has changed since the database was created. Consider using Code First Migrations to update the database"
            Database.SetInitializer<GridLayoutModelObjectContext>(null);
          
           Database.ExecuteSqlCommand(CreateDatabaseInstallationScript());
           SaveChanges();
        }

        public void Uninstall()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("DROP TABLE PWGridLayout_GroupToPage;");
            sb.Append("DROP TABLE PWGridLayout_Item;");
            sb.Append("DROP TABLE PWGridLayout_Group;");            

           Database.ExecuteSqlCommand(sb.ToString());
           SaveChanges();
        }

        public new IDbSet<TEntity> Set<TEntity>() where TEntity : BaseEntity
        {
            return base.Set<TEntity>();
        }

        public System.Collections.Generic.IList<TEntity> ExecuteStoredProcedureList<TEntity>(string commandText, params object[] parameters) where TEntity : BaseEntity, new()
        {
            throw new System.NotImplementedException();
        }

        public System.Collections.Generic.IEnumerable<TElement> SqlQuery<TElement>(string sql, params object[] parameters)
        {
            throw new System.NotImplementedException();
        }

        public int ExecuteSqlCommand(string sql, int? timeout = null, params object[] parameters)
        {
            throw new System.NotImplementedException();
        }

        public int ExecuteSqlCommand(string sql, bool tf, int? timeout = null, params object[] parameters)
        {
            throw new System.NotImplementedException();
        }
    }
}